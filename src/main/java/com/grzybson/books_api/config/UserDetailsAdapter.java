package com.grzybson.books_api.config;

import com.grzybson.books_api.model.User;
import com.grzybson.books_api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsAdapter implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        System.out.println("UserDetailsServiceAdapter called for username " + username);
        User user = userRepository.findByLogin(username);

        if (user == null) {
            throw new UsernameNotFoundException("User with login " + username + " does not exist!");
        }
        String userRole = user.isAdmin() ? "ADMIN" : "USER";

        return org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password("{noop}" + user.getPassword())
                .authorities("ROLE_" + userRole)
                .build();
    }
}
