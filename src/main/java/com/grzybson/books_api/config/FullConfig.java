package com.grzybson.books_api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class FullConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsAdapter userDetailsAdapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/gfx/**").permitAll()
                .antMatchers(HttpMethod.POST).permitAll()
                .antMatchers("/registerUser").permitAll()
                .antMatchers(HttpMethod.GET, "/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT, "/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/loginPage.html")
                .usernameParameter("login")
                .passwordParameter("password")
                .failureForwardUrl("/badLog.html")
                .successForwardUrl("/welcome")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/logout.html")
                .permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/unauthorized.html");
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider dap = new DaoAuthenticationProvider();
        dap.setUserDetailsService(userDetailsAdapter);
//        dap.setPasswordEncoder(new BCryptPasswordEncoder());
        auth.authenticationProvider(dap);
    }
}
