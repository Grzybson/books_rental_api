package com.grzybson.books_api.controllers;

import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.bookDtos.CreateBookDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookInvalidDataException;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.services.BookService;
import com.grzybson.books_api.BooksRentalApplication;
import com.grzybson.books_api.dto.bookDtos.UpdateBookDto;
import com.grzybson.books_api.service.exceptions.lendExceptions.LenderNotDefinedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BooksRentalApplication.API_V1 + "/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public List<BookDto> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/byIsbn/{isbn}")
    public BookDto getBookByIsbn(@PathVariable String isbn) throws BookNotFound {
        return bookService.getBookByIsbnNumber(isbn);
    }

    @GetMapping("/byTitle/{title}")
    public List<BookDto> getBookByTitle(@PathVariable String title) throws BookNotFound {
        return bookService.getBookByTitle(title);
    }

    @GetMapping("/book/{bookId}/lender")
    public UserDto getBookLender(@PathVariable String bookId) throws BookNotFound, LenderNotDefinedException {
        return bookService.getBookLender(bookId);
    }

    @PostMapping
    public BookDto createBook(@RequestBody CreateBookDto bookDto) throws BookInvalidDataException, UserNotFound {
        return bookService.createBook(bookDto);
    }

    @PutMapping("/{isbn}")
    public BookDto updateBook(@RequestBody UpdateBookDto bookDto, @PathVariable String isbn)
            throws BookNotFound, BookInvalidDataException {
        return bookService.updateBookByIsbn(bookDto, isbn);
    }

    @DeleteMapping("/byIsbn/{isbn}")
    public BookDto deleteBookByIsbn(@PathVariable String isbn) throws BookNotFound {
        return bookService.deleteBookByIsbn(isbn);
    }

//    @DeleteMapping("/byTitle/{title}")
//    public BookDto deleteBookByTitle(@PathVariable String title) throws BookNotFound {
//        return bookService.deleteBookByTitle(title);
//    }
}
