package com.grzybson.books_api.controllers;

import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LendNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LenderNotDefinedException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.services.LendService;
import com.grzybson.books_api.BooksRentalApplication;
import com.grzybson.books_api.dto.lendDto.CreateLendDto;
import com.grzybson.books_api.dto.lendDto.LendDto;
import com.grzybson.books_api.dto.lendDto.UpdateLendDto;
import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.service.exceptions.lendExceptions.LendInvalidDataValueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BooksRentalApplication.API_V1 + "/lends")
public class LendController {

    @Autowired
    private LendService lendService;

    @GetMapping
    public List<LendDto> getAllLends() {
        return lendService.getAllLends();
    }

    @GetMapping("/{lendId}")
    public LendDto getLend(@PathVariable String lendId) throws LendNotFound {
        return lendService.getLendById(lendId);
    }

    @PostMapping
    public LendDto createLend(@RequestBody CreateLendDto lendDto, CreateUserDto userDtoId)
            throws LendInvalidDataValueException {
        return lendService.createLend(lendDto, userDtoId);
    }

    @PutMapping("/{lendId}")
    public LendDto updateLend(UpdateLendDto lendDto, @PathVariable String lendId) throws LendNotFound {
        return lendService.updateLendById(lendDto, lendId);
    }

    @DeleteMapping("/{lendId}")
    public LendDto deleteLend(@PathVariable String lendId) throws LendNotFound {
        return lendService.deleteLendById(lendId);
    }

    @GetMapping("/user/{userId}/books")
    public List<BookDto> getAllLendBooksByUser(String userId) throws UserNotFound {
        return lendService.getAllLenderBooks(userId);
    }

    @PostMapping("/user/{userId}/book/{bookId}")
    public UserDto lendBookByUser(@PathVariable String userId, @PathVariable String bookId) throws UserNotFound, BookNotFound {
        return lendService.lendBookByUser(userId, bookId);
    }

    @DeleteMapping("/user/{userId}/book/{bookId}")
    public UserDto removeBookFromLender(@PathVariable String lenderId, @PathVariable String bookId) throws BookNotFound, LenderNotDefinedException {
        return lendService.giveBackBookByLender(lenderId, bookId);
    }
}
