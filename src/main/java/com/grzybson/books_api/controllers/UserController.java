package com.grzybson.books_api.controllers;

import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.service.exceptions.userExceptions.UserAlreadyExist;
import com.grzybson.books_api.service.exceptions.userExceptions.UserHaveSomeBooksException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserInvalidDataException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.services.UserService;
import com.grzybson.books_api.BooksRentalApplication;
import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.dto.userDtos.UpdateUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BooksRentalApplication.API_V1 + "/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{userId}")
    public UserDto getUserById(@PathVariable String userId) throws UserNotFound {
        return userService.getUserById(userId);
    }

    @GetMapping("/{login}")
    public CreateUserDto getUserByLogin(@PathVariable String login) throws UserNotFound {
        return userService.getUserByLogin(login);
    }

    @PostMapping
    public UserDto createUser(@RequestBody CreateUserDto userDto)
            throws UserAlreadyExist, UserInvalidDataException {
        return userService.createUser(userDto);
    }

    @PutMapping("/{login}")
    public UpdateUserDto updateUser(@RequestBody UpdateUserDto userDto, @PathVariable String id)
            throws UserNotFound, UserInvalidDataException {
        return userService.updateUserById(userDto, id);
    }

    @DeleteMapping("/{userId}")
    public UserDto deleteUserById(@PathVariable String userId) throws UserNotFound, UserHaveSomeBooksException {
        return userService.deleteUserById(userId);
    }

}
