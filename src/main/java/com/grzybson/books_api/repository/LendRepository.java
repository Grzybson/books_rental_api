package com.grzybson.books_api.repository;

import com.grzybson.books_api.model.Lend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LendRepository extends JpaRepository<Lend, String> {
}
