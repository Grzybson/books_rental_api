package com.grzybson.books_api.service.mappers;

import com.grzybson.books_api.dto.lendDto.CreateLendDto;
import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.model.Lend;
import com.grzybson.books_api.service.exceptions.lendExceptions.LendInvalidDataValueException;
import com.grzybson.books_api.dto.lendDto.LendDto;
import org.springframework.stereotype.Component;

import java.util.UUID;


@Component
public class LendDtoMapper {

    public LendDto mapToDto(Lend lend) {
        return LendDto.builder()
                .id(lend.getId())
                .bookId(lend.getBookId())
                .userId(lend.getUserId())
                .startDate(lend.getStartDate())
                .returnDate(lend.getReturnDate())
                .build();
    }

    public Lend mapToModel(CreateLendDto lendDto, CreateUserDto userId) {
        return new Lend(
                UUID.randomUUID().toString(),
                lendDto.getBookId(),
                userId.getId(),
                lendDto.getStartDate(),
                lendDto.getReturnDate());
    }

    public void validate(CreateLendDto lendDto) throws LendInvalidDataValueException {
        if (lendDto.getBookId() == null || lendDto.getUserId() == null || lendDto.getStartDate() == null) {
            throw new LendInvalidDataValueException();
        }

    }
}
