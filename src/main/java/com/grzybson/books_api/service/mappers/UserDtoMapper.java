package com.grzybson.books_api.service.mappers;

import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.dto.userDtos.UpdateUserDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.model.User;
import com.grzybson.books_api.service.exceptions.userExceptions.UserInvalidDataException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Component
public class UserDtoMapper {
    public UserDto mapToDto(User user) {
        List<String> booksIds = user.getBooks().stream()
                .map(b -> b.getIsbn())
                .collect(Collectors.toList());

        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .name(user.getName())
                .surname(user.getSurname())
                .city(user.getCity())
                .birthYear(user.getBirthYear())
                .booksIds(booksIds)
                .build();

    }

    public CreateUserDto mapToLoginUser(User user) {

        return CreateUserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .name(user.getName())
                .surname(user.getSurname())
                .city(user.getCity())
                .birthYear(user.getBirthYear())
                .booksIds(user.getBooks().stream().map(b -> b.getIsbn()).collect(Collectors.toList()))
                .build();

    }

    public User mapToModel(CreateUserDto userDto) {

        return new User(
                UUID.randomUUID().toString(),
                userDto.getLogin(),
                userDto.getPassword(),
                userDto.getName(),
                userDto.getSurname(),
                userDto.getCity(),
                userDto.getBirthYear(),
                new ArrayList<>());
    }

    public void validate(@RequestBody CreateUserDto userDto) throws UserInvalidDataException {
        if (userDto.getLogin() == null || userDto.getLogin().isEmpty() || userDto.getLogin().length() < 3
                || userDto.getPassword() == null || userDto.getPassword().isEmpty() || userDto.getPassword().length() < 5) {
            throw new UserInvalidDataException();
        }

    }
    // mapping needed to update User from html

    public UpdateUserDto mapToUpdate(User user) {

        return UpdateUserDto.builder()
                .id(user.getId())
                .password(user.getPassword())
                .name(user.getName())
                .surname(user.getSurname())
                .city(user.getCity())
                .birthYear(user.getBirthYear())
                .build();

    }
}
