package com.grzybson.books_api.service.mappers;


import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.bookDtos.CreateBookDto;
import com.grzybson.books_api.model.Book;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookInvalidDataException;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class BookDtoMapper {

    public BookDto mapToDto(Book book) {
        String lenderId = book.getLender() != null ? book.getLender().getLogin() : null;
        return new BookDto(book.getIsbn(),
                book.getTitle(),
                book.getAuthor(),
                book.getGenre(),
                book.getYear(),
                book.getPublisher(),
                lenderId);

    }

    public Book mapToModel(CreateBookDto bookDto) {
        return new Book(
                UUID.randomUUID().toString(),
                bookDto.getTitle(),
                bookDto.getAuthor(),
                bookDto.getGenre(),
                bookDto.getYear(),
                bookDto.getPublisher(),
                null);

    }

    public void validate(CreateBookDto bookDto) throws BookInvalidDataException {
        if (bookDto.getIsbn() == null) {
            throw new BookInvalidDataException();
        }

    }

}
