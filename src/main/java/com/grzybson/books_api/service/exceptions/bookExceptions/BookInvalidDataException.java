package com.grzybson.books_api.service.exceptions.bookExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BookInvalidDataException extends Exception {
    private static final String MESSAGE = "Bad book value. ";

    public BookInvalidDataException() {
        super(MESSAGE);
    }
}
