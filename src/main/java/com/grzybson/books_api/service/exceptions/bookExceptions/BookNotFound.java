package com.grzybson.books_api.service.exceptions.bookExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BookNotFound extends Exception {
    private static final String MESSAGE = "Can not find book with this parameter. ";

    public BookNotFound() {
        super(MESSAGE);
    }
}
