package com.grzybson.books_api.service.exceptions.lendExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LenderNotDefinedException extends Exception {
    private static final String MESSAGE = "Can not find any lender of this book. ";

    public LenderNotDefinedException() {
        super(MESSAGE);
    }
}
