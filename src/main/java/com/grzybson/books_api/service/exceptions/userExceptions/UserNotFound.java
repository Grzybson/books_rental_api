package com.grzybson.books_api.service.exceptions.userExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFound extends Exception {
    private static final String MESSAGE = "Can not find User with this value. ";

    public UserNotFound() {
        super(MESSAGE);
    }
}
