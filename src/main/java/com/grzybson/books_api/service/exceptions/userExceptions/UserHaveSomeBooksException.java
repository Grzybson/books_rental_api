package com.grzybson.books_api.service.exceptions.userExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserHaveSomeBooksException extends Exception {
    private static final String MESSAGE = "Can not delete this user, they have lend books";

    public UserHaveSomeBooksException() {
        super(MESSAGE);
    }
}
