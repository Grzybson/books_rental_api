package com.grzybson.books_api.service.exceptions.userExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserAlreadyExist extends Exception {
    private static final String MESSAGE = "User with that login already exist. ";

    public UserAlreadyExist() {
        super(MESSAGE);
    }
}
