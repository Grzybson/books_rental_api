package com.grzybson.books_api.service.exceptions.userExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserInvalidDataException extends Exception {
    private static final String MESSAGE = "Null or to short value in login or password field";

    public UserInvalidDataException() {
        super(MESSAGE);
    }

}
