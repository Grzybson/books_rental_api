package com.grzybson.books_api.service.exceptions.lendExceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LendInvalidDataValueException extends Exception {
    private static final String MESSAGE = "Bad value/values in create method. ";

    public LendInvalidDataValueException() {
        super(MESSAGE);
    }
}
