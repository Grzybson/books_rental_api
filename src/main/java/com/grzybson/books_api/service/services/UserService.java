package com.grzybson.books_api.service.services;

import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.repository.BookRepository;
import com.grzybson.books_api.repository.UserRepository;
import com.grzybson.books_api.service.exceptions.userExceptions.UserAlreadyExist;
import com.grzybson.books_api.service.exceptions.userExceptions.UserHaveSomeBooksException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserInvalidDataException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.dto.userDtos.UpdateUserDto;
import com.grzybson.books_api.model.Book;
import com.grzybson.books_api.model.User;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LenderNotDefinedException;
import com.grzybson.books_api.service.mappers.BookDtoMapper;
import com.grzybson.books_api.service.mappers.UserDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private UserDtoMapper userMapper;
    @Autowired
    private BookDtoMapper bookDtoMapper;

    @Transactional
    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream()
                .map(u -> userMapper.mapToDto(u))
                .collect(Collectors.toList());
    }

    @Transactional
    public UserDto getUserById(String userId) throws UserNotFound {
        return userRepository.findById(userId)
                .map(u -> userMapper.mapToDto(u))
                .orElseThrow(() -> new UserNotFound());
    }

    @Transactional
    public CreateUserDto getUserByLogin(String login) throws UserNotFound {
        User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new UserNotFound();
        }
        userRepository.save(user);
        CreateUserDto userToLog = userMapper.mapToLoginUser(user);
        return userToLog;

    }

    @Transactional
    public List<BookDto> getBookByTitle(String title) throws BookNotFound {
        if (bookRepository.findByTitle(title) == null) {
            throw new BookNotFound();
        }
        return bookRepository.findByTitle(title)
                .stream()
                .map(bookDtoMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public UpdateUserDto methodToUpdateGetUserById(String id) throws UserNotFound {
        return userRepository.findById(id)
                .map(user -> userMapper.mapToUpdate(user))
                .orElseThrow(UserNotFound::new);
    }

    @Transactional
    public UserDto createUser(CreateUserDto userDto) throws UserInvalidDataException, UserAlreadyExist {
        userMapper.validate(userDto);

        if (userRepository.existsById(userDto.getLogin())) {
            throw new UserAlreadyExist();
        }
        User newUser = userMapper.mapToModel(userDto);
        newUser.setPassword(newUser.getPassword().trim());

        User savedUser = userRepository.save(newUser);

        return userMapper.mapToDto(savedUser);
    }

    @Transactional
    public UpdateUserDto updateUserById(UpdateUserDto userDto, String id) throws UserNotFound, UserInvalidDataException {
        if (userDto.getPassword() == null || userDto.getPassword().length() < 5) {
            throw new UserInvalidDataException();
        }
        User updateUser = userRepository.findById(id).orElseThrow(() -> new UserNotFound());
        updateUser.setPassword(userDto.getPassword());
        updateUser.setName(userDto.getName());
        updateUser.setSurname(userDto.getSurname());
        updateUser.setCity(userDto.getCity());
        updateUser.setBirthYear(userDto.getBirthYear());

        User saveduser = userRepository.save(updateUser);

        return userMapper.mapToUpdate(saveduser);
    }


    @Transactional
    public UserDto deleteUserById(String userId) throws UserNotFound, UserHaveSomeBooksException {
        User userToDelete = userRepository.findById(userId).orElseThrow(() -> new UserNotFound());
        if (!userToDelete.getBooks().isEmpty()) {
            throw new UserHaveSomeBooksException();
        }
        userRepository.delete(userToDelete);
        return userMapper.mapToDto(userToDelete);
    }

}




