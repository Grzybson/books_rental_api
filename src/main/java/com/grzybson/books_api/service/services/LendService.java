package com.grzybson.books_api.service.services;

import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.lendDto.CreateLendDto;
import com.grzybson.books_api.dto.lendDto.LendDto;
import com.grzybson.books_api.dto.lendDto.UpdateLendDto;
import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.model.Book;
import com.grzybson.books_api.model.Lend;
import com.grzybson.books_api.model.User;
import com.grzybson.books_api.repository.BookRepository;
import com.grzybson.books_api.repository.LendRepository;
import com.grzybson.books_api.repository.UserRepository;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LendInvalidDataValueException;
import com.grzybson.books_api.service.exceptions.lendExceptions.LendNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LenderNotDefinedException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.mappers.BookDtoMapper;
import com.grzybson.books_api.service.mappers.LendDtoMapper;
import com.grzybson.books_api.service.mappers.UserDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LendService {

    @Autowired
    private LendRepository lendRepository;
    @Autowired
    private LendDtoMapper lendDtoMapper;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDtoMapper userDtoMapper;

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookDtoMapper bookDtoMapper;

    @Transactional
    public List<LendDto> getAllLends() {
        return lendRepository.findAll().stream()
                .map(lend -> lendDtoMapper.mapToDto(lend))
                .collect(Collectors.toList());

    }

    @Transactional
    public LendDto getLendById(String id) throws LendNotFound {
        return lendRepository.findById(id)
                .map(l -> lendDtoMapper.mapToDto(l))
                .orElseThrow(() -> new LendNotFound());

    }

    @Transactional
    public LendDto createLend(CreateLendDto lendDto, CreateUserDto userDtoId) throws LendInvalidDataValueException {
        lendDtoMapper.validate(lendDto);

        Lend newLend = lendDtoMapper.mapToModel(lendDto, userDtoId);
        Lend savedlend = lendRepository.save(newLend);
        return lendDtoMapper.mapToDto(savedlend);
    }

    @Transactional
    public LendDto updateLendById(UpdateLendDto lendDto, String id) throws LendNotFound {
        Lend updateLend = lendRepository.findById(id).orElseThrow(() -> new LendNotFound());
        updateLend.setBookId(lendDto.getBookId());
        updateLend.setUserId(lendDto.getUserId());
        updateLend.setReturnDate(lendDto.getReturnDate());

        Lend savedUpdateLend = lendRepository.save(updateLend);

        return lendDtoMapper.mapToDto(savedUpdateLend);
    }

    @Transactional
    public LendDto deleteLendById(String id) throws LendNotFound {
        Lend deletelend = lendRepository.findById(id).orElseThrow(() -> new LendNotFound());
        lendRepository.delete(deletelend);
        return lendDtoMapper.mapToDto(deletelend);
    }

    @Transactional
    public List<BookDto> getAllLenderBooks(String userId) throws UserNotFound {
        User lender = userRepository.findById(userId).orElseThrow(() -> new UserNotFound());

        return lender.getBooks().stream()
                .map(b -> bookDtoMapper.mapToDto(b))
                .collect(Collectors.toList());


    }

    @Transactional
    public UserDto lendBookByUser(String userId, String bookId) throws UserNotFound, BookNotFound {
        User user = userRepository.findById(userId).orElseThrow(UserNotFound::new);
        Book book = bookRepository.findById(bookId).orElseThrow(BookNotFound::new);

        user.addBook(book);

        User savedUser = userRepository.save(user);

        return userDtoMapper.mapToDto(savedUser);
    }

    @Transactional
    public UserDto giveBackBookByLender(String lenderId, String bookId) throws LenderNotDefinedException, BookNotFound {
        User lender = userRepository.findById(lenderId).orElseThrow(LenderNotDefinedException::new);
        Book givinBackBook = bookRepository.findById(bookId).orElseThrow(BookNotFound::new);

        lender.giveBackBook(givinBackBook);

        User saveduser = userRepository.save(lender);

        return userDtoMapper.mapToDto(saveduser);
    }
}
