package com.grzybson.books_api.service.services;

import com.grzybson.books_api.dto.bookDtos.CreateBookDto;
import com.grzybson.books_api.repository.BookRepository;
import com.grzybson.books_api.repository.LendRepository;
import com.grzybson.books_api.repository.UserRepository;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.bookDtos.UpdateBookDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.model.Book;
import com.grzybson.books_api.model.User;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookInvalidDataException;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LenderNotDefinedException;
import com.grzybson.books_api.service.mappers.BookDtoMapper;
import com.grzybson.books_api.service.mappers.UserDtoMapper;
import com.grzybson.books_api.view.dto.ViewLendBookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookDtoMapper bookDtoMapper;
    @Autowired
    private UserDtoMapper userDtoMapper;


    @Transactional
    public List<BookDto> getAllBooks() {
        return bookRepository.findAll().stream()
                .map(b -> bookDtoMapper.mapToDto(b))
                .collect(Collectors.toList());
    }

    @Transactional
    public BookDto getBookByIsbnNumber(String isbn) throws BookNotFound {
        return bookRepository.findById(isbn)
                .map(b -> bookDtoMapper.mapToDto(b))
                .orElseThrow(() -> new BookNotFound());
    }

    @Transactional
    public List<BookDto> getBookByTitle(String title) throws BookNotFound {
        if (bookRepository.findByTitle(title) == null) {
            throw new BookNotFound();
        }
        return bookRepository.findByTitle(title)
                .stream()
                .map(bookDtoMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public UserDto getBookLender(String isbn) throws LenderNotDefinedException, BookNotFound {
        Book book = bookRepository.findById(isbn).orElseThrow(() -> new BookNotFound());

        User lender = book.getLender();
        if (lender == null) {
            throw new LenderNotDefinedException();
        }
        return userDtoMapper.mapToDto(lender);
    }

    @Transactional
    public BookDto createBook(CreateBookDto bookDto) throws BookInvalidDataException, UserNotFound {
        bookDtoMapper.validate(bookDto);
        Book newBook = bookDtoMapper.mapToModel(bookDto);
        Book savedBook = bookRepository.save(newBook);
        return bookDtoMapper.mapToDto(savedBook);

    }

    @Transactional
    public BookDto updateBookByIsbn(UpdateBookDto bookDto, String isbn) throws BookNotFound, BookInvalidDataException {
        Book bookToUpdate = bookRepository.findById(isbn).orElseThrow(() -> new BookNotFound());
        bookToUpdate.setTitle(bookDto.getTitle());
        bookToUpdate.setAuthor(bookDto.getAuthor());
        bookToUpdate.setGenre(bookDto.getGenre());
        bookToUpdate.setYear(bookDto.getYear());
        bookToUpdate.setPublisher(bookDto.getPublisher());

        Book savedBook = bookRepository.save(bookToUpdate);

        return bookDtoMapper.mapToDto(savedBook);
    }

    @Transactional
    public BookDto deleteBookByIsbn(String isbn) throws BookNotFound {

        Book bookToDelete = bookRepository.findById(isbn).orElseThrow(() -> new BookNotFound());
        bookRepository.delete(bookToDelete);
        return bookDtoMapper.mapToDto(bookToDelete);
    }

    @Transactional
    public BookDto lendBookByUserWithIsbn(ViewLendBookDto bookDto, String isbn) throws BookNotFound, BookInvalidDataException {
        Book bookToLend = bookRepository.findById(isbn).orElseThrow(() -> new BookNotFound());
        bookToLend.setIsbn(bookDto.getIsbn());
        bookToLend.setTitle(bookDto.getTitle());
        bookToLend.setAuthor(bookDto.getAuthor());
        bookToLend.setGenre(bookDto.getGenre());
        bookToLend.setYear(bookDto.getYear());
        bookToLend.setPublisher(bookDto.getPublisher());
        bookToLend.setLender(userRepository.getOne(bookDto.getLenderId()));

        Book savedBook = bookRepository.save(bookToLend);

        return bookDtoMapper.mapToDto(savedBook);
    }


}
