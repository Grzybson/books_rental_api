package com.grzybson.books_api.dto.userDtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {

    private String id;
    private String login;
    private String name;
    private String surname;
    private String city;
    private int birthYear;
    private List<String> booksIds = new ArrayList<>();
}
