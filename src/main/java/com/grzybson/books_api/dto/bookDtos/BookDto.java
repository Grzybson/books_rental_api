package com.grzybson.books_api.dto.bookDtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookDto {

    private String isbn;
    @NotNull
    @Size(min = 3)
    private String title;
    @NotEmpty
    private String author;
    @NotEmpty
    private String genre;
    @NotNull
    private int year;
    @NotEmpty
    private String publisher;
    @NotBlank
    private String lenderId;
}
