package com.grzybson.books_api.dto.lendDto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateLendDto {

    private String bookId;
    private String userId;
    private java.sql.Date returnDate;

}
