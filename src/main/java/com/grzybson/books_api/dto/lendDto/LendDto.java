package com.grzybson.books_api.dto.lendDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LendDto {

    private String id;
    @NotEmpty
    private String bookId;
    @NotEmpty
    private String userId;
    @NotNull
    private java.sql.Date startDate;
    private java.sql.Date returnDate;

}
