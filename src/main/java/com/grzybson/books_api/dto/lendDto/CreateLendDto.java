package com.grzybson.books_api.dto.lendDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateLendDto {

    private String id;
    private String bookId;
    private String userId;
    private java.sql.Date startDate;
    private java.sql.Date returnDate;

}
