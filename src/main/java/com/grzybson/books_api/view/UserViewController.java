package com.grzybson.books_api.view;

import com.grzybson.books_api.dto.userDtos.CreateUserDto;
import com.grzybson.books_api.dto.userDtos.UpdateUserDto;
import com.grzybson.books_api.dto.userDtos.UserDto;
import com.grzybson.books_api.service.exceptions.userExceptions.UserAlreadyExist;
import com.grzybson.books_api.service.exceptions.userExceptions.UserHaveSomeBooksException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserInvalidDataException;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.services.UserService;
import com.grzybson.books_api.view.dto.ViewUpdateUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.util.Set;

@Controller
public class UserViewController {

    @Autowired
    private UserService userService;

    @Autowired
    private Validator validator;


    @GetMapping("/users-list")
    public ModelAndView displayLendersTable() {
        ModelAndView modelAndView = new ModelAndView("lenders");
        modelAndView.addObject("lenders", userService.getAllUsers());
        UserDto userDto = new UserDto();
        modelAndView.addObject("searchUser");

        return modelAndView;
    }

    @GetMapping("/searchedLender")
    public ModelAndView displaySearchedLender(@RequestParam String login) throws UserNotFound {
        ModelAndView modelAndView = new ModelAndView("searchLender");
        modelAndView.addObject("lender", userService.getUserByLogin(login));

        return modelAndView;
    }

    @PostMapping("/searchLender")
    public ModelAndView showSearchLender(@ModelAttribute("searchUser") UserDto userDto) throws UserNotFound {
        Set<ConstraintViolation<UserDto>> constraintViolation = validator.validate(userDto);
        if(constraintViolation.isEmpty()){}
        final CreateUserDto lender = userService.getUserByLogin(userDto.getLogin());
        ModelAndView modelAndView = new ModelAndView("searchLender");
        modelAndView.addObject("lender", lender);
        return modelAndView;
    }

    @GetMapping("/createUser")
    public ModelAndView displayUserCreateForm() {
        CreateUserDto createUserDto = new CreateUserDto();
        createUserDto.setBirthYear(1970);

        ModelAndView modelAndView = new ModelAndView("createUser");
        modelAndView.addObject("userToCreate", createUserDto);
        return modelAndView;
    }

    @PostMapping("/createUser")
    public String createUser(@Valid @ModelAttribute(name = "userToCreate") CreateUserDto createUserDto, BindingResult bindingResult)
            throws UserInvalidDataException, UserAlreadyExist {
        if(bindingResult.hasErrors()){
            return "createUser";
        }
        userService.createUser(createUserDto);
        return "redirect:/users-list";
    }

    @GetMapping("/updateUser")
    public ModelAndView displayUserUpdateForm(@RequestParam String userId) throws UserNotFound {
        final UpdateUserDto user = userService.methodToUpdateGetUserById(userId);
        ViewUpdateUserDto updateUserDto = ViewUpdateUserDto.viewUserBuilder()
                .id(user.getId())
                .password(user.getPassword())
                .name(user.getName())
                .surname(user.getSurname())
                .city(user.getCity())
                .birthYear(user.getBirthYear())
                .build();
        ModelAndView modelAndView = new ModelAndView("updateUser");
        modelAndView.addObject("userToUpdate", updateUserDto);
        return modelAndView;
    }

    @PostMapping("/updateUser")
    public String updateUser(@Valid @ModelAttribute(name = "userToUpdate") ViewUpdateUserDto viewUpdateUserDto,
                             BindingResult bindingResult) throws UserNotFound, UserInvalidDataException {
        if(bindingResult.hasErrors()){
            return "updateUser";
        }
        userService.updateUserById(viewUpdateUserDto, viewUpdateUserDto.getId());
        return "redirect:/users-list";
    }

    @GetMapping("/deleteUser")
    public String deleteUser(@RequestParam String userId) {
        try {
            userService.deleteUserById(userId);
        } catch (UserNotFound | UserHaveSomeBooksException user) {
            user.printStackTrace();
        }
        return "redirect:/users-list";
    }

    @GetMapping("/registerUser")
    public ModelAndView displayUserRegistryForm() {
        CreateUserDto userToRegistry = new CreateUserDto();
        userToRegistry.setBirthYear(1970);

        ModelAndView modelAndView = new ModelAndView("registerUser");
        modelAndView.addObject("userToRegistry", userToRegistry);
        return modelAndView;
    }

    @PostMapping("/registerUser")
    public String registryUser(CreateUserDto userDto) throws UserInvalidDataException, UserAlreadyExist {
        userService.createUser(userDto);

        return "redirect:/loginPage";
    }

}
