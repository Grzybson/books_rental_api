package com.grzybson.books_api.view;

import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainViewController {

    @Autowired
    private UserService userService;

    @PostMapping("/welcome")
    public String openWelcome(){
        return "redirect:/welcomeInLibrary";
    }


    @GetMapping("/welcomeIn")
    public ModelAndView goToMainPage(){
        ModelAndView modelAndView = new ModelAndView("/loginPage.html");
        modelAndView.addObject("positiveLogin");
        return modelAndView;
    }

    @PostMapping("/welcomeIn/{login}")
    public String loginToMainPage(@PathVariable String login) throws UserNotFound {
        if(userService.getUserByLogin(login)  == null ){
            throw new UserNotFound();
        }
        return "redirect:/welcomeInLibrary";
    }

    @GetMapping("/welcomeInLibrary")
    public ModelAndView showWelcomePage(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("welcome");
        modelAndView.addObject("login", authentication.getName());
        return modelAndView;
    }




//    @GetMapping("/confirmationLogout")
//    public ModelAndView confirmationUserLogout() {
//        ModelAndView modelAndView = new ModelAndView("/confirmationLogoutPage");
//        modelAndView.addObject("confirmedLogin");
//        return modelAndView;
//    }
//
//    @PostMapping("/confirmLogout")
//    public String logoutFromApp(@RequestParam String login, Authentication authentication) throws UserNotFound {
//        if(userService.getUserByLogin(login) == null){
//            throw new UserNotFound();
//        }
//        ModelAndView modelAndView = new ModelAndView("logout");
//        modelAndView.addObject("login", authentication.getName());
//        return "redirect:/logout.html";
//    }
//
//    @GetMapping("/logoutFromApp")
//    public ModelAndView showLogoutPage(Authentication authentication) {
//        ModelAndView modelAndView = new ModelAndView("logout");
//        modelAndView.addObject("login", authentication.getName());
//        return modelAndView;
//    }


}
