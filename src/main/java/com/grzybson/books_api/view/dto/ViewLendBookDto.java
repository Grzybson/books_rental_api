package com.grzybson.books_api.view.dto;

import com.grzybson.books_api.dto.bookDtos.BookDto;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor

public class ViewLendBookDto extends BookDto {

    @Builder(builderMethodName = "viewBookBuilder")
    public ViewLendBookDto(String isbn, String title, String author, String genre, int year, String publisher,
                           String lenderId) {
        super(isbn, title, author, genre, year, publisher, lenderId);

    }

}
