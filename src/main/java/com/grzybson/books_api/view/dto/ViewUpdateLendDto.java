package com.grzybson.books_api.view.dto;


import com.grzybson.books_api.dto.lendDto.UpdateLendDto;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ViewUpdateLendDto extends UpdateLendDto {
    private String id;

    @Builder(builderMethodName = "viewLendBuilder")
    public ViewUpdateLendDto(String bookId, String userId, java.sql.Date returnDate, String id) {
        super(userId, bookId, returnDate);
        this.id = id;
    }

}
