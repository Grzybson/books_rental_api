package com.grzybson.books_api.view.dto;

import com.grzybson.books_api.dto.bookDtos.UpdateBookDto;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ViewUpdateBookDto extends UpdateBookDto {
    private String isbn;

    @Builder(builderMethodName = "viewBookBuilder")
    public ViewUpdateBookDto(String title, String author, String genre, int year, String publisher, String isbn) {
        super(title, author, genre, year, publisher);
        this.isbn = isbn;
    }
}
