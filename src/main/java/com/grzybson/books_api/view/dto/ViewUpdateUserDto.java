package com.grzybson.books_api.view.dto;

import com.grzybson.books_api.dto.userDtos.UpdateUserDto;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor

public class ViewUpdateUserDto extends UpdateUserDto {


    @Builder(builderMethodName = "viewUserBuilder")
    public ViewUpdateUserDto(String id, String password, String name, String surname, String city, int birthYear) {
        super(id, password, name, surname, city, birthYear);

    }

}
