package com.grzybson.books_api.view;


import com.grzybson.books_api.dto.bookDtos.BookDto;
import com.grzybson.books_api.dto.bookDtos.CreateBookDto;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookInvalidDataException;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.userExceptions.UserNotFound;
import com.grzybson.books_api.service.services.BookService;
import com.grzybson.books_api.view.dto.ViewLendBookDto;
import com.grzybson.books_api.view.dto.ViewUpdateBookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import javax.validation.*;
import java.util.List;
import java.util.Set;

@Controller
public class BookViewController {


    @Autowired
    private BookService bookService;
    @Autowired
    private Validator validator ;


    @GetMapping("/books-list")
    public ModelAndView displayBooksTable() {
        ModelAndView modelAndView = new ModelAndView("library");
        modelAndView.addObject("books", bookService.getAllBooks());
        BookDto bookDto = new BookDto();
        modelAndView.addObject("searchBook");

        return modelAndView;
    }

    @GetMapping("/searchedBook-list")
    public ModelAndView displaySearchedBook(@RequestParam String title) throws BookNotFound {
        ModelAndView modelAndView = new ModelAndView("searchBookList");
        modelAndView.addObject("books", bookService.getBookByTitle(title));

        return modelAndView;
    }

    @PostMapping("/searchBook")
    public ModelAndView showSearchBook(@ModelAttribute("searchBook") BookDto bookDto) throws BookNotFound {
        Set<ConstraintViolation<BookDto>> constraintViolation = validator.validate(bookDto);
        if(constraintViolation.isEmpty()){}
        final List<BookDto> books = bookService.getBookByTitle(bookDto.getTitle());
        ModelAndView modelAndView = new ModelAndView("searchBookList");
        modelAndView.addObject("books", books);
        return modelAndView;
    }


    @GetMapping("/createBook")
    public ModelAndView displayBookCreateForm() {
        CreateBookDto createBookDto = new CreateBookDto();

        ModelAndView modelAndView = new ModelAndView("createBook");
        modelAndView.addObject("book", createBookDto);
        return modelAndView;
    }

    @PostMapping("/createBook")
    public String createBook(@Valid @ModelAttribute(name = "book") CreateBookDto createBookDto, BindingResult bindingResult)
            throws BookInvalidDataException, UserNotFound {
        if(bindingResult.hasErrors()){
            return "createBook";
        }
        bookService.createBook(createBookDto);
        return "redirect:/books-list";
    }


    @GetMapping("/updateBook")
    public ModelAndView displayBookUpdateForm(@RequestParam String isbn) throws BookNotFound {
        final BookDto book = bookService.getBookByIsbnNumber(isbn);
        ViewUpdateBookDto updateBookDto = ViewUpdateBookDto.viewBookBuilder()
                .isbn(book.getIsbn())
                .publisher(book.getPublisher())
                .genre(book.getGenre())
                .author(book.getAuthor())
                .title(book.getTitle())
                .year(book.getYear())
                .build();

        ModelAndView modelAndView = new ModelAndView("updateBook");
        modelAndView.addObject("viewUpdateBookDto", updateBookDto);
        return modelAndView;

    }

    @PostMapping("/updateBook")
    public String updateBook(@Valid @ModelAttribute(name = "viewUpdateBookDto") ViewUpdateBookDto viewUpdateBookDto,
                             BindingResult bindingResult) throws BookNotFound, BookInvalidDataException {
        if(bindingResult.hasErrors()){
            return "updateBook";
        }
        bookService.updateBookByIsbn(viewUpdateBookDto, viewUpdateBookDto.getIsbn());
        return "redirect:/books-list";
    }


    @GetMapping("/lendBook")
    public ModelAndView displayBookLendByUser(@RequestParam String isbn) throws BookNotFound {
        final BookDto lendBook = bookService.getBookByIsbnNumber(isbn);
        ViewLendBookDto lendBookByUser = ViewLendBookDto.viewBookBuilder()
                .isbn(lendBook.getIsbn())
                .publisher(lendBook.getPublisher())
                .genre(lendBook.getGenre())
                .author(lendBook.getAuthor())
                .title(lendBook.getTitle())
                .year(lendBook.getYear())
                .lenderId(lendBook.getLenderId())
                .build();

        ModelAndView modelAndView = new ModelAndView("lendBook");
        modelAndView.addObject("bookToLend", lendBookByUser);
        return modelAndView;
    }

    @PostMapping("/lendBook")
    public String lendBook(@Valid @ModelAttribute(name = "bookToLend") ViewLendBookDto viewLendBookDto, BindingResult bindingResult)
            throws BookNotFound, BookInvalidDataException {
        if(bindingResult.hasErrors()){
            return "lendBook";
        }
        bookService.lendBookByUserWithIsbn(viewLendBookDto, viewLendBookDto.getIsbn());
        return "redirect:/books-list";
    }


    @GetMapping("/deleteBook")
    public String deleteBook(@RequestParam String isbn) {
        try {
            bookService.deleteBookByIsbn(isbn);
        } catch (BookNotFound bookNotFound) {
            bookNotFound.printStackTrace();
        }
        return "redirect:/books-list";
    }


}
