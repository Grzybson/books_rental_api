package com.grzybson.books_api.view;

import com.grzybson.books_api.dto.lendDto.LendDto;
import com.grzybson.books_api.service.exceptions.bookExceptions.BookNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LendNotFound;
import com.grzybson.books_api.service.exceptions.lendExceptions.LenderNotDefinedException;
import com.grzybson.books_api.service.services.LendService;
import com.grzybson.books_api.view.dto.ViewUpdateLendDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;




@Controller
public class LendViewController {

    @Autowired
    private LendService lendService;


    @GetMapping("/lends-list")
    public ModelAndView displayLendsTable() {
        ModelAndView modelAndView = new ModelAndView("lends");
        modelAndView.addObject("lends", lendService.getAllLends());
        LendDto lendDto = new LendDto();
        modelAndView.addObject("searchLend");

        return modelAndView;
    }

    @GetMapping("/searchLend")
    public ModelAndView displaySearchedLend(@RequestParam String id) throws LendNotFound{
        ModelAndView modelAndView = new ModelAndView("searchLendPage");
        modelAndView.addObject("lend", lendService.getLendById(id));
        return modelAndView;
    }

    @PostMapping("/searchLend")
    public ModelAndView showSearchLend(@ModelAttribute("searchLend") LendDto lendDto) throws LendNotFound{
        final LendDto searchedLend = lendService.getLendById(lendDto.getId());
        ModelAndView modelAndView = new ModelAndView("searchLendPage");
        modelAndView.addObject("lend", searchedLend);
        return modelAndView;
    }


    @GetMapping("/updateLend/{id}")
    public ModelAndView updateLendById(@RequestParam String id) throws LendNotFound {
        final LendDto lend = lendService.getLendById(id);
        ViewUpdateLendDto updateLendDto = ViewUpdateLendDto.viewLendBuilder()
                .id(lend.getId())
                .bookId(lend.getBookId())
                .userId(lend.getUserId())
                .returnDate(lend.getReturnDate())
                .build();

        ModelAndView modelAndView = new ModelAndView("updateLend");
        modelAndView.addObject("lendToUpdate", updateLendDto);
        return modelAndView;
    }

    @PostMapping("/updateLend")
    public String updateLend(@ModelAttribute ViewUpdateLendDto viewUpdateLendDto) throws LendNotFound {
        lendService.updateLendById(viewUpdateLendDto, viewUpdateLendDto.getId());
        return "redirect:/lends-list";
    }


    @GetMapping("/giveBackBook/{lenderId}/{bookId}")
    public String giveBackBookByLender(@RequestParam String lenderId,@RequestParam String bookId) {
        try {
            lendService.giveBackBookByLender(lenderId, bookId);
        } catch (BookNotFound | LenderNotDefinedException bookNotFound) {
            bookNotFound.printStackTrace();
        }
        return "redirect:/lends-list";
    }


}
