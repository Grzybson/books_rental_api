package com.grzybson.books_api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class User {

    @Id
    @Column(columnDefinition = "varchar(100)")
    private String id;
    @NotEmpty
    @Size(min = 3, max = 20)
    private String login;
    @NotEmpty
    @Size(min = 5, max = 20)
    private String password;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotBlank
    private String city;
    @NotNull
    private int birthYear;
    @OneToMany(mappedBy = "lender", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Book> books = new ArrayList<>();


    public void addBook(Book book) {
        this.books.add(book);
        book.setLender(this);
    }

    public void giveBackBook(Book givinBackBook) {
        this.books.remove(givinBackBook);
        givinBackBook.setLender(null);
    }


    public boolean isAdmin() {
        return login.equalsIgnoreCase("admin");
    }
}
