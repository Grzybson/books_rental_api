package com.grzybson.books_api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(of = "isbn")
public class Book {

    @Id
    @Column(columnDefinition = "varchar(100)")
    private String isbn;
    @NotNull
    @Size(min = 3)
    private String title;
    @NotEmpty
    private String author;
    @NotEmpty
    private String genre;
    @NotNull
    private int year;
    @NotEmpty
    private String publisher;
    @ManyToOne
    private User lender;

}
