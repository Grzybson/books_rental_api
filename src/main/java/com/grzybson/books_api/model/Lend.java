package com.grzybson.books_api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Lend {

    @Id
    @Column(columnDefinition = "varchar(100)")
    private String id;
    @NotEmpty
    private String bookId;
    @NotEmpty
    private String userId;
    @NotNull
    private java.sql.Date startDate;
    private java.sql.Date returnDate;


}
