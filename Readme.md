## About project:
#### (For the time being a pre-beta version of app, several features need improvement)
This is a "simple library" application.

After login into application by our login & password, 
we can lend books, lengthen the lend and of course give back book or update our account.

The admin has more possibilities, he can delete, add, update books and lends, 
he can also delete and add users/lenders.

## Getting Started
First of all Instal MySQL 

After `git clone` from repository make empty database in MySQL and named it e.g. books_rental 
or named otherwise but then fix datasource.url in properties.

##### How to create database:
* `create database books_rental`;
* `use books_rental`;

Another things to run application is to make properties for hibernate to connect
into our database to do ORM.

#### Properties:
* spring.jpa.hibernate.ddl-auto=update
* spring.datasource.url=jdbc:mysql://localhost:3306/books_rental?autoReconnect=true&useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true
* spring.datasource.username=root
* spring.datasource.password=root
* spring.jpa.show-sql=true

To see that connection to databse in IntelliJ is succeed,
click database icon in right taskbar and click on + ,
mark Data Source and choose our SQL e.g. MySQL

Now set database source and drives like this:

![Alt text](/gfx/databaseDrives.jpg)

It should be like this after connection:

![Alt text](/gfx/schemas.jpg)

If there not, do refresh.

##### Now app is ready to run.

## How to run application
After project setup, search main page: http://localhost:8080/loginPage.html
Register yourself or log into app as admin - login:admin, password:admin

## Technologies
Project is created with:
#### IDE
* IntelliJ IDEA
#### Languages       
* Java 8
* HTML
* CSS
* Thymeleaf
#### Tools
* Spring MVC
* JPA
* Hibernate

